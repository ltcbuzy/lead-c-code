# README: What is the customer need? #

When we want to start a business, the most important thing in this direction is to have the ability to pre-design the customer buying process and take the first step to identify their needs. We must always pay attention to how we create needs in the customer.

### What is the customer need? ###

What needs can the customer have and how does our product meet their needs? This question can be asked in the mind and the appropriate answer can be reached.
Customer needs are the motivation that motivates the customer to buy a product or service. Ultimately, need is the focus of a customer's purchasing decision. Most companies see customer needs as an opportunity to eliminate or involve excess value in restoring core motivation.
An example of customer needs happens every day around 12 noon. That is, when people experience hunger (need) and decide to buy lunch.
The type of food, the location of the restaurant and the amount of time your service lasts are all factors that show how people decide to meet their needs.

### How to create customer needs###
* Quick summary
* Version
* [US TRAFFIC]( https://www.targetedwebtraffic.com/buy/state-targeted-usa-website-traffic/)
Sometimes you have to sell something that people do not know they need. Maybe they do not need it now and will need it later.
In this case, you have to create a sense of need in them. This is different from understanding the needs of customers and is actually creating a need for them.

## Stressing technique ###

In this situation, you need to make customers feel a certain amount of anxiety and worry. If you work in insurance, use this method. For example, ask the customer: Do you have life insurance?
What insurance sellers do is actually make you really think about the pain! Maybe you never think about that pain yourself and maybe that painful thing will not happen to you at all! But those sellers force you to think of different aspects!
Then when you feel the pain, the sellers say, "You really do not want this to happen, do you?" Think that with only 50,000 months, you can be sure that none of this will happen

### Some call this the stress-relief technique. ###

First, they create a stressful image in your mind: "My God! "My wife cannot feed the family!
But then by saying: "But do not worry! "This insurance will solve your problem!" They relieve you of pain.

### Take advantage of customer weakness ###

For example, suppose you want to persuade a company president to sign up for your negotiation course. You can ask him about the company's profit and if he complains about the low profit, tell him: Well, why do you think your company's profit is low?!
He may say: "I honestly think we cannot get money from our customers very well.
Here you have to tell him: What exactly do you mean? Can you give me an example?
This is exactly where you need to remind your boss of the weakness of that company in the negotiation and tell him about the benefits of participating in the negotiation period. And you have to emphasize that they have so far lost their profits because they do not know how to negotiate, and by participating in this course, their problem will be completely solved.
Make the customer know that they really need to do something to meet their needs
What you need to do is ask questions. And that you make the customer come to the conclusion that: we must do it!

### You have to ask some people: ###

* How much will it cost to repair your car if you have an accident?
* Or how much does it cost to drive [US State Targeted Traffic to your website]( https://www.targetedwebtraffic.com/buy/state-targeted-usa-website-traffic/)?
Usually, they did not think about these questions. You can ask them about the efficiency of their employees. How much does it cost you every time you hire a new staff? How much do you spend on them per year?!
* And they get scared by thinking about the answer and a simple account and book!
* Your job as a sales specialist is to use this method in your field of expertise.
* Then ask yourself: How can I stress my client? How can I create a need in their minds?
* If you do this, then when you see your proposed solution, they will have to accept your offer.
Just remember that this is not a lie at all. These needs really exist and people are willing to spend money to ease their minds about such issues.

You're just trying to get customers to really think about their needs and evaluate them. And realize that they have to do something to meet their needs

### Awareness through advertising ###

By persuading the customer to buy, you can make them aware of their need for the product or service. The best way to create a customer need and motivate them to buy is to inform them through personal advertising and sales. How?
Convince the customer; Talk about the benefits of your product or service, speak to the customer's needs, and try to show that you know their needs and that you have the right solution for them.
Showcase your product; Do not neglect crowded places. 


* Try to use appropriate photos and videos to prove the effectiveness of your product or service. This will make your potential customers think and create customer needs.
Be creative and use new ways to promote your product / service; For example, you can submit your product ad to a popular blogger or offer your products to him for free and ask him to comment if he is satisfied.
Allow the customer to use your product or service for free. 
* Set a time limit (for example, 30 days) for this possibility and let the customer return it to you after this period if he is not satisfied with the performance of the product.
* The [FAQ]( https://www.targetedwebtraffic.com/faq/) section on the website can be very helpful. 
* One of the customer needs is easy access to information. Adding this section to the website can help convince the customer faster.
Of course, there are other reasons as well, such as quality product (their drink is of the best quality), coherent advertising, strong distribution networks, strong management skills, but most of it was due to the influence of influential people that Puff Didi, Sirook's owner, from It used to market  this brand. 
* In fact, from the day Didi started owning the brand, he has been in the spotlight by including his name in videos, TV shows, high-profile events and photos.
So if your target market is imitating their mental idols, why not get them to promote your brand?
In the USA, some brands have used this issue in their advertisements and by using the influence of Iranian celebrities, they persuade the target market to use their brand.

### Create a sense of customer satisfaction ###

In the first case, we mentioned that we should create fear in the customer, but in this case, we want to point out that sometimes to create a need in the customer, we have to show the customer the enjoyment of something.

For example, if you can start your own traditional business online and enter the digital world, what opportunities can you get and what benefits can you get?

## Conclusion ##
So be sure to use this method. The basis of business formation is meeting the needs of the people. People need a product or service, so businesses are set up to meet those needs. But we know that human needs are limited; So what to do? The answer is simple: create a need in the customer.


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team [contact us](https://angela-s-school-bizm.thinkific.com/courses/your-first-course)
